package msadur.virtualoffice.controller;


import msadur.virtualoffice.dto.ProductDto;
import msadur.virtualoffice.model.Product;
import msadur.virtualoffice.model.employees.Worker;
import msadur.virtualoffice.service.ProductService;
import msadur.virtualoffice.service.WorkerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class Controller {

    private ProductService productService;
    private WorkerService workerService;

    public Controller(ProductService productService, WorkerService workerService) {
        this.productService = productService;
        this.workerService = workerService;
    }

    @GetMapping(value = "/")
    public ModelAndView homePage() {
        ModelAndView mav = new ModelAndView("home");
        return mav;
    }


    @GetMapping(value = "/products")
    public List<ProductDto> getAllProducts() {
        return productService.getAll();
    }

}
