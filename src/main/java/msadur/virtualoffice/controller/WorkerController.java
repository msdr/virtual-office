package msadur.virtualoffice.controller;

import msadur.virtualoffice.dto.NewWorkerDto;
import msadur.virtualoffice.dto.WorkerDto;
import msadur.virtualoffice.model.employees.Worker;
import msadur.virtualoffice.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class WorkerController {

    @Autowired
    private WorkerService workerService;


    @GetMapping(value = "/employees")
    public ModelAndView getEmployees() {
        ModelAndView mav = new ModelAndView("worker/list");
        mav.addObject("workers", workerService.getEmployees());
        return mav;
    }

    @GetMapping(value = "/employee/create")
    public ModelAndView create() {
        ModelAndView mav = new ModelAndView(("worker/create"));
        mav.addObject("form", new NewWorkerDto());
        mav.addObject("workers", workerService.getEmployees());
        return mav;
    }

    @PostMapping(value = "/employee/create")
    public String createFromDto(@ModelAttribute NewWorkerDto dto) {
        workerService.createWorker(dto);
        return "redirect:/employees";
    }

    @GetMapping(value = "/employee/edit")
    public ModelAndView edit(@RequestParam Long id){
        ModelAndView mav = new ModelAndView("worker/edit");
        mav.addObject("form", workerService.getEmployeeById(id));
        mav.addObject("workers", workerService.getEmployees());
        return mav;
    }

    @PostMapping(value = "/employee/edit")
    public String update(@ModelAttribute WorkerDto dto){
        workerService.editWorker(dto);
        return "redirect:/employees?id=" + dto.getId();
    }
}
