package msadur.virtualoffice.controller;

import msadur.virtualoffice.repository.OrderRepository;
import msadur.virtualoffice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OrderController {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    OrderService orderService;

    @GetMapping(value = "order/list")
    public ModelAndView getOrders(){
        ModelAndView mav = new ModelAndView("order/list");
        mav.addObject("orders", orderService.getOrders());
        return mav;
    }


}
