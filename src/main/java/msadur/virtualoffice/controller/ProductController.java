package msadur.virtualoffice.controller;

import msadur.virtualoffice.dto.ProductDto;
import msadur.virtualoffice.model.Product;
import msadur.virtualoffice.repository.ProductRepository;
import msadur.virtualoffice.service.ProductMapper;
import msadur.virtualoffice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;


    @GetMapping(value = "/product/list")
    public ModelAndView getProducts() {
        ModelAndView mav = new ModelAndView("product/list");
        mav.addObject("products", productService.getAll());
        return mav;
    }

    @GetMapping(value = "/product/create")
    public ModelAndView createProduct() {
        ModelAndView mav = new ModelAndView("product/create");
        mav.addObject("form", new ProductDto());
        mav.addObject("products", productService.getAll());
        return mav;
    }

    @PostMapping(value = "/product/create")
    public String createFromDto(@ModelAttribute ProductDto dto) {
    productService.createFromDto(dto);
    return "redirect:/product/list";
    }
}