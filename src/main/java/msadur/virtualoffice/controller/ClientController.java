package msadur.virtualoffice.controller;

import msadur.virtualoffice.repository.ClientRepository;
import msadur.virtualoffice.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ClientService clientService;


    @GetMapping(value = "client/list")
    public ModelAndView getClients() {
        ModelAndView mav = new ModelAndView("client/list");
        mav.addObject("clients", clientService.getClients());
        return mav;
    }
}
