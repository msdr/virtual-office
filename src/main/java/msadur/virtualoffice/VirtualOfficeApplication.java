package msadur.virtualoffice;

import msadur.virtualoffice.helpingTools.InsertHelperSQL;
import msadur.virtualoffice.model.Product;
import msadur.virtualoffice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class VirtualOfficeApplication {



	public static void main(String[] args) {
		SpringApplication.run(VirtualOfficeApplication.class, args);

	}
}
