package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.WorkerDto;
import msadur.virtualoffice.model.employees.Worker;
import org.springframework.stereotype.Component;

@Component
public class WorkerMapper {

    public WorkerDto mapFromObject(Worker worker) {
        WorkerDto dto = new WorkerDto();
        dto.setId(worker.getId());
        dto.setFirstName(worker.getFirstName());
        dto.setLastName(worker.getLastName());
        dto.setEmploymentDate(worker.getEmploymentDate().toString());
        dto.setSalary(worker.getSalary());
        dto.setDepartment(worker.getDepartment());
        return dto;
    }
}
