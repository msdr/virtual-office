package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.ProductDto;
import msadur.virtualoffice.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    public ProductDto mapFromObject(Product product) {
        ProductDto dto = new ProductDto();
        dto.setId(product.getId());
        dto.setName(product.getName());
        dto.setAttribute(product.getAttribute());
        dto.setPrice(product.getPrice());
        return dto;
    }

    public Product mapFromDto(ProductDto dto){
        Product product = new Product();
        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        product.setAttribute(dto.getAttribute());
        return product;
    }
}
