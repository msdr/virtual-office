package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.NewWorkerDto;
import msadur.virtualoffice.dto.WorkerDto;
import msadur.virtualoffice.model.employees.Department;
import msadur.virtualoffice.model.employees.OfficeWorker;
import msadur.virtualoffice.model.employees.Worker;
import msadur.virtualoffice.repository.WorkerRepository;
import net.bytebuddy.build.Plugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkerService {

    @Autowired
    private WorkerRepository workerRepository;
    @Autowired
    private WorkerMapper workerMapper;


    public Worker createWorker(NewWorkerDto dto) {
        Department dep = dto.getDepartment();
        if (dep.equals(Department.OFFICE)) {
            return workerRepository.save(new OfficeWorker(dto));
        } else if (dep.equals(Department.WAREHOUSE)) {
            return null;
        } else throw new IllegalStateException("coś nie tak");
    }

    public List<WorkerDto> getEmployees() {
        return workerRepository.findAll().stream()
                .map(worker -> workerMapper.mapFromObject(worker))
                .collect(Collectors.toList());
    }

    public WorkerDto getEmployeeById(Long id) {
        return workerRepository.findById(id)
                .map(worker -> workerMapper.mapFromObject(worker))
                .orElseThrow(() -> new IllegalStateException("can't find worker with that id"));
    }

    public void editWorker(WorkerDto dto) {
        Worker workerToEdit = workerRepository.getById(dto.getId());
        workerToEdit.setId(dto.getId());
        workerToEdit.setFirstName(dto.getFirstName());
        workerToEdit.setLastName(dto.getLastName());
        workerToEdit.setEmploymentDate(LocalDate.parse(dto.getEmploymentDate()));
        workerToEdit.setSalary(dto.getSalary());
        workerToEdit.setDepartment(dto.getDepartment());
        workerRepository.save(workerToEdit);
    }

    public List<Worker> getEmployeesRest() {
        return workerRepository.findAll();
    }
}
