package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.OrderDto;
import msadur.virtualoffice.model.Order;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class OrderMapper {

    public OrderDto mapFromObject(Order order) {
        OrderDto dto = new OrderDto();
        dto.setId(order.getId());
        dto.setPrice(order.getPrice());

      /*  dto.setProductList(
                order.getProductList().stream()
                .map(product -> product.getId())
                .collect(Collectors.toList()));
*/

        return dto;
    }
}
