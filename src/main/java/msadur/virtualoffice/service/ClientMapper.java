package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.ClientDto;
import msadur.virtualoffice.model.Client;
import org.springframework.stereotype.Component;

@Component
public class ClientMapper {

    public ClientDto mapFromObject(Client client) {
        ClientDto dto = new ClientDto();
        dto.setId(client.getId());
        dto.setFirstName(client.getFirstName());
        dto.setLastName(client.getLastName());
        dto.setCompany(client.getCompany());
        dto.setWorker(client.getWorker().getFirstName() + " " + client.getWorker().getLastName());
        return dto;
    }

}
