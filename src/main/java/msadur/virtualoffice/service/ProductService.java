package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.ProductDto;
import msadur.virtualoffice.model.Product;
import msadur.virtualoffice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    ProductMapper productMapper;


    public List<ProductDto> getAll() {
        return productRepository.findAll()
                .stream()
                .map(product -> productMapper.mapFromObject(product))
                .collect(Collectors.toList());
    }

    public Product createFromDto(ProductDto dto) {
        Product product = productMapper.mapFromDto(dto);
        return productRepository.save(product);
    }

}
