package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.ClientDto;
import msadur.virtualoffice.model.Client;
import msadur.virtualoffice.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ClientMapper clientMapper;

    public List<ClientDto> getClients() {
        return clientRepository.findAll().
                stream()
                .map(client -> clientMapper.mapFromObject(client))
                .collect(Collectors.toList());
    }

}
