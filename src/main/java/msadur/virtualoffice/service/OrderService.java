package msadur.virtualoffice.service;

import msadur.virtualoffice.dto.OrderDto;
import msadur.virtualoffice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    OrderRepository orderRepository;


    public List<OrderDto> getOrders() {
        return orderRepository.findAll()
                .stream()
                .map(order -> orderMapper.mapFromObject(order))
                .collect(Collectors.toList());
    }

}
