package msadur.virtualoffice.helpingTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class InsertHelperSQL {


    public InsertHelperSQL() {
    }


    public String designClientQueryForDB(int firstId, int lastId,int workerId, String tableName){

        StringBuilder stringBuilder = new StringBuilder();
        String queryPart1 = "INSERT INTO `";
        String queryPart2 ="` (`id`, `first_name`, `last_name`, `company`, `worker_id`) " + "VALUES (";

        for(int i = firstId; i <= lastId; i ++)
        {
            stringBuilder.append(queryPart1);
            stringBuilder.append(tableName);
            stringBuilder.append(queryPart2);
            stringBuilder.append(i + ", ");
            stringBuilder.append( "'John"+ i + "', ");
            stringBuilder.append( "'Kovalsky"+ i + "', ");
            stringBuilder.append( "'Very Important Company"+ i + "', ");
            stringBuilder.append( workerId + "); " + "\r\n");
        }
        return  stringBuilder.toString();
    }

}
