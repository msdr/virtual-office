package msadur.virtualoffice.model.employees;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public enum Department {
    @JsonProperty("WAREHOUSE")
    WAREHOUSE,
    @JsonProperty("OFFICE")
    OFFICE;


    public static Department getDepartment(String departmentCode) {
        return Arrays.stream(Department.values())
                .filter(value -> value.name().equals(departmentCode))
                .findFirst()
                .orElse(Department.OFFICE);
    }

}
