package msadur.virtualoffice.model.employees;

import msadur.virtualoffice.dto.NewWorkerDto;
import msadur.virtualoffice.model.Client;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("OFFICE")
public class OfficeWorker extends Worker{

    @OneToMany(fetch = FetchType.LAZY)
    private List<Client> clientList;

    public OfficeWorker() {
    }

    public OfficeWorker(NewWorkerDto dto) {
        super(
                dto.getFirstName(),
                dto.getLastName(),
                LocalDate.now(),
                dto.getSalary(),
                Department.OFFICE
        );
        this.clientList = new ArrayList<>();
    }

    public OfficeWorker(List<Client> clientList) {
        this.clientList = clientList;
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }
}
