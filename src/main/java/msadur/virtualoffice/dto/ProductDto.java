package msadur.virtualoffice.dto;

import java.math.BigDecimal;

public class ProductDto {

    private Long id;
    private String name;
    private String attribute;
    private BigDecimal price;

    public ProductDto() {
    }

    public ProductDto(Long id, String name,
                      String attribute, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.attribute = attribute;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
