package msadur.virtualoffice.dto;

import msadur.virtualoffice.model.employees.Department;

import java.math.BigDecimal;
import java.time.LocalDate;

public class NewWorkerDto {

    private String firstName;
    private String lastName;
    private BigDecimal salary;
    private Department department;

    public NewWorkerDto() {
    }

    public NewWorkerDto(String firstName, String lastName, BigDecimal salary, Department department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.department = department;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
