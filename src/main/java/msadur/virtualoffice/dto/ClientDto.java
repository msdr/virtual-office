package msadur.virtualoffice.dto;

import msadur.virtualoffice.model.Order;
import msadur.virtualoffice.model.employees.Worker;

import java.util.List;

public class ClientDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String company;
    private String worker;

    public ClientDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }
}
