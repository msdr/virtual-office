package msadur.virtualoffice.dto;

import java.math.BigDecimal;
import java.util.List;

public class OrderDto {

    private Long id;
    private BigDecimal price;
    private String Client;
    //private List<Long> productList;
    private String client;
    public OrderDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getClient() {
        return Client;
    }

    public void setClient(String client) {
        Client = client;
    }

}
